package service

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"time"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// service
	service *common.Service

	// is running?
	isRunning bool

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// rabbit mq controller
	rabbitMQController *rabbit.Controller

	// rabbit mq consumer
	rabbitMQConsumer <-chan amqp.Delivery

	// mongoDB handler
	mongoHandler *Mongo
}

func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceSensor].Name),
		isRunning: true,
	}

	// build rabbit mq connection
	if rabbitMQController, err := rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err == nil {
		service.rabbitMQController = rabbitMQController
	} else {
		return nil, err
	}
	if _, err := service.rabbitMQController.Channel.QueueDeclare(rabbit.SensorQueueName,
		true,
		false,
		false,
		false,
		nil); err != nil {
		return nil, err
	}

	if mongoHandler, err := BuildMongo(configuration); err == nil {
		service.mongoHandler = mongoHandler
	} else {
		return nil, errors.New( "mongo: " +
			err.Error( ) )
	}

	// run update thread
	go service.updateThread()

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceSensor,
		SensorAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}

// close service
func (service *Service) Close() {
	service.isRunning = false
	service.rabbitMQConsumer = nil
	service.rabbitMQController.Close()
	service.mongoHandler = nil
	service.mongoHandler.Close()
}

// update
func (service *Service) updateThread() {
	for service.isRunning {
		if channel := service.rabbitMQController.Channel; channel != nil {
			var err error
			if service.rabbitMQConsumer, err = channel.Consume(rabbit.SensorQueueName,
				"",
				false,
				false,
				false,
				false,
				nil); err == nil {
				for message := range service.rabbitMQConsumer {
					if err := service.mongoHandler.HandleIncomingMessage(string(message.Body)); err != nil {
						fmt.Println( err )
					}
					_ = channel.Ack(message.DeliveryTag,
						false)
				}

			}
		}

		// sleep
		time.Sleep(time.Second)
	}
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}
