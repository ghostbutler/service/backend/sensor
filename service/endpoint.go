package service

import (
	"gitlab.com/ghostbutler/tool/service"
)

// define your services ids, must start from last built in index
const (
	// login based on an OTP
	// use to contact the service from outside of the
	// internal zone
	// delivers a session ID valid for a given period
	//APIServiceV1Sensor = common.APIServiceType(iota + common.APIServiceBuiltInLast)
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var SensorAPIService = map[common.APIServiceType]*common.APIEndpoint{
}
