package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/globalsign/mgo"
	"gitlab.com/ghostbutler/tool/service/device"
	"time"
)

type Mongo struct {
	Session          *mgo.Session
	Database         string
	SensorCollection string
}

func BuildMongo(configuration *Configuration) (*Mongo, error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    configuration.MongoDB.Hostname,
		Timeout:  10 * time.Second,
		Database: configuration.MongoDB.Authentication.Database,
		Username: configuration.MongoDB.Authentication.UserName,
		Password: configuration.MongoDB.Authentication.Password,
	}
	if mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo); err != nil {
		return nil, err
	} else {
		mongoSession.SetMode(mgo.Monotonic, true)
		return &Mongo{
			Session:          mongoSession,
			Database:         configuration.MongoDB.Database,
			SensorCollection: configuration.MongoDB.SensorCollection,
		}, nil
	}
}

func (mongo *Mongo) Close() {
	mongo.Session.Close()
}

func (mongo *Mongo) HandleIncomingMessage(message string) error {
	sessionCopy := mongo.Session.Copy()
	defer sessionCopy.Close()

	collection := sessionCopy.DB(mongo.Database).C(mongo.SensorCollection)

	var m device.Export
	_ = json.Unmarshal([]byte(message), &m)

	var sensorData device.SensorData
	sensorData.Type = m.Type
	sensorData.Time, _ = time.Parse(time.RFC3339Nano,
		m.Time)
	switch m.Type {
	case device.CapabilityTypeName[device.CapabilityTypeLight]:
		var exp device.Light
		_ = json.Unmarshal([]byte(m.Content), &exp)

		sensorData.Data = exp
		sensorData.Name = exp.Name

		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break
	case device.CapabilityTypeName[device.CapabilityTypeOutlet]:
		var exp device.Outlet
		_ = json.Unmarshal([]byte(m.Content), &exp)
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break
	case device.CapabilityTypeName[device.CapabilityTypeGamepad]:
		var exp device.Switch
		_ = json.Unmarshal([]byte(m.Content), &exp)
		gamepadEvent, _ := json.Marshal(exp.Event)
		switch exp.Type {
		case device.SwitchEventTypeButton:
			var button device.Button
			_ = json.Unmarshal(gamepadEvent, &button)
			exp.Event = button
			break

		case device.SwitchEventTypeJoystick:
			var joystick device.Joystick
			_ = json.Unmarshal(gamepadEvent, &joystick)
			exp.Event = joystick
			break
		}
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break

	case device.CapabilityTypeName[device.CapabilityTypeMusic]:
		var exp device.MusicEvent
		_ = json.Unmarshal([]byte(m.Content), &exp)
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break

	case device.CapabilityTypeName[device.CapabilityTypeTextToSpeech]:
		var exp device.TTS
		_ = json.Unmarshal([]byte(m.Content), &exp)
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break
	case device.CapabilityTypeName[device.CapabilityTypeTemperature]:
		var exp device.TemperatureEvent
		_ = json.Unmarshal([]byte(m.Content), &exp)
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break
	case device.CapabilityTypeName[device.CapabilityTypeMoisture]:
		var exp device.MoistureEvent
		_ = json.Unmarshal([]byte(m.Content), &exp)
		sensorData.Data = exp
		sensorData.Name = exp.Name
		if err := collection.Insert(sensorData); err != nil {
			return err
		}
		break

	default:
		return errors.New("unknown event type")
	}
	fmt.Println("saving event for",
		sensorData.Type,
		", name =",
		sensorData.Name,
		":",
		m.Content)
	return nil
}
