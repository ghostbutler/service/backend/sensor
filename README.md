Sensor service
==============

Introduction
------------

The sensor service is the sensor queue consumer. It read controller status updates
from queue, and persist it to database.

Configuration
-------------

```json
{
	"securityManager": {
		"hostname": "securitymanager.ghostbutler",
		"username": "GhostButler2018Scanner",
		"password": "GhostButlerProject2018*"
	},
	"rabbitmq": {
		"hostname": [ "127.0.0.1:5672" ],
		"username": "guest",
		"password": "guest"
	},
	"mongodb": {
	  	"auth": {
		  	"username": "admin",
		  	"password": "admin",
		  	"database": "admin"
		},
		"hostname": [ "127.0.0.1:27017" ],
		"database": "ghostbutler",
	  	"sensorCollection": "sensor"
	}
}
```

Author
------

- SALINAS Joseph <salinasjo92@gmail.com>

https://gitlab.com/ghostbutler/service/backend/sensor

Contributor
-----------

- SOARES Lucas <lucas.soares@orange.com>
